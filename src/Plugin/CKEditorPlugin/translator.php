<?php

namespace Drupal\translator\Plugin\CKEditorPlugin;

use Drupal\translator\Plugin\TranslatorEcosystemCKEditorPlugin;
use Drupal\editor\Entity\Editor;

/**
 * Defines the plugin.
 *
 * @CKEditorPlugin(
 *   id = "Translator",
 *   label = @Translation("Translator"),
 *   module = "translator"
 * )
 */
class translator extends TranslatorEcosystemCKEditorPlugin
{

    /**
     * {@inheritdoc}
     */
    public function getButtonsDef()
    {
        return array(
            'Translator' => array(
                'label' => 'Translate selected text',
                'image' => 'https://cdn.n1ed.com/cdn/buttons/Translator.png'
            ),
            'TranslatorConf' => array(
                'label' => 'Translation settings',
                'image' => 'https://cdn.n1ed.com/cdn/buttons/TranslatorSettings.png'
            ),
            'TranslatorReverse' => array(
                'label' => 'Translation direction reverce',
                'image' => 'https://cdn.n1ed.com/cdn/buttons/TranslatorReverse.png'
            ),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPluginName()
    {
        return "Translator";
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleName()
    {
        return "translator";
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor)
    {
        return array(
            "N1ED"
        );
    }

    /**
     * {@inheritdoc}
     */
    public function addControlsToForm(&$form, $editor, $config)
    {
        $this->addBooleanToForm($form, $config, "enableTranslator", true);
        $this->addJsonToForm($form, $config, "Translator", "");
    }
}
